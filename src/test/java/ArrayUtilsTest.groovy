import org.junit.Test

import static org.junit.Assert.assertEquals

class ArrayUtilsTest {

    @Test
    public void shouldReturnEmptyListWhenNullInput() {
        assertEquals(0, ArrayUtils.flat(null).size())
    }

    @Test
    public void shouldReturnEmptyLListWhenEmptyInput() {
        assertEquals(0, ArrayUtils.flat([]).size())
    }

    @Test
    public void shouldFlatArray() {
        assertEquals([1, 2, 3, 4], ArrayUtils.flat([[1, 2, [3]], 4]))
    }

    @Test
    public void shouldKeepArrayAsItIsWhenNoNestedArrays() {
        assertEquals([3, 2, 1], ArrayUtils.flat([3, 2, 1]))
    }

}
