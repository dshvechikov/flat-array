/**
 * Created by dima on 06.09.15.
 */
class ArrayUtils {

    def static flat(def array) {
        if (array == null) {
            return []
        }
        def result = []
        flatArray(array, result)
        result
    }

    private static Object flatArray(array, result) {
        array.each { el ->
            if (isCollectionOrArray(el)) {
                flatArray(el, result)
            } else {
                result.add(el)
            }
        }
        result
    }

    def static isCollectionOrArray(object) {
        [Collection, Object[]].any { it.isAssignableFrom(object.getClass()) }
    }
}
